

TIMEZONE = 1

function showArc(start, stop, colorarray) 
    -- print("plotting an arc from "..start.." to "..stop)
    -- print("on "..NUMLEDS.." leds")

    local startfraction = (start*NUMLEDS) % 1;
    local startled      = math.floor(start*NUMLEDS+1) % NUMLEDS;      -- +1 to ceil 

    local stopfraction  = (stop*NUMLEDS) % 1;
    local stopled       = math.floor(stop*NUMLEDS) % NUMLEDS;        -- stop is floored 
    
    local fadestartled = (startled-1+NUMLEDS) % NUMLEDS;              -- the led before startled is fadein
    local fadestopled  = (stopled)            % NUMLEDS;              -- stopled itself is the fadeout led

    if fadestartled == fadestopled then 
        if stop < start then
            for i=0,NUMLEDS-1 do
                colorarray[i+1] = 255;  
            end
        end
    else
    	i = startled
        while i ~= stopled do
        	colorarray[i+1] = 255;
        	i=(i+1)%NUMLEDS
        end
    end
    

    local startlvl = 255 - (startfraction*255);
    local stoplvl  = stopfraction*255; 

   
    if fadestartled == fadestopled then
        local together = startlvl + stoplvl; 
        if start < stop then together = 255*(stopfraction-startfraction) end
        -- print("single pixel fade: ", start, stop, together)
        -- print("start: ",start, startled, fadestartled,startfraction, startlvl)
        -- print("stop: ", stop, stopled, fadestopled,stopfraction, stoplvl)
        colorarray[fadestartled+1] = together;
 	else
        colorarray[fadestartled+1] = startlvl;
        colorarray[fadestopled+1]  = stoplvl;
    end
end



function display_time() 
	local unix, micros,i
    unix,micros = rtctime.get()
	-- print("unixtime: ",unix)
	-- print("micros: ",micros)

	local hours   = math.floor((unix / 3600) + TIMEZONE) % 12 -- used to be 24
	local minutes = math.floor(unix / 60) % 60
	local seconds = unix % 60
	local millis  = math.floor(micros /1000)

	-- print("time: "..hours..":"..minutes..":"..seconds.."+"..millis)

    local minutesAngle = minutes/60 + seconds/3600 + millis/3600000;
    local secondsAngle = seconds/60 + millis/60000;
    local hoursAngle   = hours/12 + minutes/(12*60);
    

    for i=1,NUMLEDS do
        red[i] = 0; 
        green[i] = 0; 
        -- blue[i] = 0;
    end


    local phase =  minutes%2 == 0 and minutesAngle > secondsAngle
          or minutes%2 == 1 and secondsAngle > minutesAngle;
    

    if phase then
        showArc(minutesAngle,secondsAngle,red);
    else
        showArc(secondsAngle,minutesAngle,red);
    end


    showArc(0,hoursAngle,green);

    -- print("red: ")
    -- for i=0,NUMLEDS-1 do
    -- 	print("    ",i,": ",red[i])
    -- end

    
    for i=1,NUMLEDS do
    	bytes[3*ROTATE(i)-1] = math.floor(red[i])
    	bytes[3*ROTATE(i)-2] = math.floor(green[i])
    	bytes[3*ROTATE(i)  ] = 0 --math.floor(blue[i])
    end
  
    local rgb = string.char(unpack(bytes))  -- <- HERE IS THE HUGE TEMPORARY MEMORY HOG!!!
    ws2812.write(3, rgb, 3*NUMLEDS) 
    -- not really necessary:
    -- rgb = nil
    -- collectgarbage()

end

