function handlerequest(client,request)
    if string.find(request," / ") == nil then
        print("request discarded for other than /")
        return
    end
    local buf = "";

    local ssid, password, _, _ =wifi.sta.getconfig()

    local postssid = request:match("ssid=(%w+)")
    local postpasswd = request:match("password=(%w+)")
    print("request:",request)
    print("new ssid: ",postssid)
    print("new passwd: ",postpasswd)
    if postssid ~= nil and postpasswd ~= nil then
        ssid = postssid
        password = postpasswd
        wifi.sta.config(ssid,password)
        tmr.delay(100000) -- for good measure
        node.restart()
    end

    buf = buf.."<!DOCTYPE html><html><head>";
    buf = buf.."<meta name='viewport' content='width=device-width, initial-scale=1.0' />";
    buf = buf.."<title>LedClock Configuration</title>";
    buf = buf.."<style type='text/css'>";
    buf = buf.."body{background-color:#f0f0f0;color:#111;width:auto;display:block;}";
    buf = buf.."h1{font-size:200%;font-family:monospace;}";
    buf = buf.."h1,h2,p{text-align:center;}";
    buf = buf.."*{font-family:arial; }";
    buf = buf..".mono {font-family: monospace; }";
    buf = buf.."</style></head><body>";
    buf = buf.."<h2>LedClock settings</h2>";
    buf = buf.."<form method='POST' action='/'>";
    buf = buf.."<table align='center'>";
    buf = buf.."<tr><td>Connect to SSID: </td><td><input type='text' name='ssid' value='"..ssid.."' /></td></tr>";
    buf = buf.."<tr><td>with password: </td><td><input type='password' name='password' value='"..password.."' /></td></tr>";
    buf = buf.."<tr><td>Timezone offset:</td><td><input type='text' name='timezone' value='+1' /></td></tr>";
    buf = buf.."<tr><td>weather location:</td><td><input type='input' name='location' value='366,429' /></td></tr>";
    buf = buf.."<tr><td></td><td><input type='submit' /></td></tr>";
    buf = buf.."</table></form></html>";

    client:send(buf);
end