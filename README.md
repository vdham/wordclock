# Fork of ledclock

This repository is a fork of the ledclock project, to create a word clock instead. The idea is to use much of the same software and programmable components, to get a slightly different end result where leds are used to highlight words that tell the time.

![Final Result](final_result.jpg)

# Build instructions #

This project has been inspired by many other word clock projects out there. Like many of those projects, we've taken a slightly new twist on things, as the hardware available keeps changing. The main changes of this project are the use of NodeMCU and WS2812 led ribbons.

### Requirements/parts: ###
These are the parts needed, I'm linking to AliExpress but other places are probably good as well.

* Leds. This is probably the most expensive part. To fit the frame I used the ribbon with 60 leds per meter, you need at least 1,5 meter [aliexpress](http://www.aliexpress.com/item/1m-4m-5m-WS2812B-Smart-led-pixel-strip-Black-White-PCB-30-60-144-leds-m/32265421310.html)
* NodeMCU ESP8266 based node, WeMos D1 mini [wemoss.cc](http://wemos.cc) [aliexpress](http://www.aliexpress.com/item/D1-mini-Mini-NodeMcu-4M-bytes-Lua-WIFI-Internet-of-Things-development-board-based-ESP8266/32529101036.html)
* 5V power supply. This needs to be rather hefty. I used a 5A version because I wanted to be really sure that even if I accidentally turned all leds full white it wouldn't break. If you are feeling brave you could probably calculate the maximum number of leds that can be on at a time and choose a slightly smaller supply but that's up to you.  [aliexpress](http://www.aliexpress.com/item/DC-5V-5A-power-supply-AC-100-240V-switch-power-supply-EU-power-adapter-1-years/32389846563.html)
* Ikea RIBBA frame 23x23 [ikea](http://www.ikea.com/nl/nl/catalog/products/00078051/)
* A spool of thin shielded copper wire, you will need to do some soldering
* Cardboard, to create a frame for preventing bleeding of light beneath the printed words
* Depending on your desired finish, you may need some power connector
Optional:
* 1000uF capacitor to smooth the 5v line (the ledstrip can create big fluctuations that the esp really doesn't like). 
* 100 ohm resistor

### Assembly: ###

I started with a print out of the clock face to stick the led ribbon to. This makes sure they are aligned properly. The alignment does not need to be perfect, as the cardboard frame will help you with that later.

I started with four leds at the bottom, which in the future will be used for additional precision of time. Then for each row you cut off a piece of 10 leds, just use a pair of pliers to cut through the copper contacts of the ribbon.

![Wired Led Strip](wired_led_strip.jpeg)

Once the ribbon has been attached to the paper, it is time to start soldering. There are basically three lines that need to be soldered:

- 5V line, extend this from the bottom line of four on the left side, branching out to each of the lines
- Ground line, extend this from the bottom line of four on the right side, branching out to each of the lines
- Logic line, extend this from the bottom line, snaking through the lines, so the bottom line of 10 connects on the left to the line of four, and on the right to the line above it. That one then connects on the left to the one above that, and so on.

Extending the ground and 5v each on a side makes the distribution of power a bit more even.

Print another version of the clock face so that you can build the cardboard frame. Find some cardboard, and start cutting out pieces of around 17cm in length and about 2,5cm in height. These can then be hotglued together to make a square frame. Then insert a separator for each of the lines, and hotglue those to the square frame. Finally cut out small separators for each of the words.

![Cardboard Frame](cardboard_frame.jpeg)

For the connection, I used the connector from the LED strip to create the connection to the NodeMCU. GND and +5v are indicated on the board. This old picture connects to D2, the newer firmware requires that the connection is to D4, this puts 5V, GND and logic all right next to each other.

Finally drill a hole in the RIBBA frame to connect the power. Inside the frame the 5V and Ground should be connected to the Ribbon as well as the NodeMCU chip. I used a power connector within the frame.


![Hole And Nodemcu](hole_and_nodemcu.jpeg)

### firmware ###

My current recommended firmware is an Arduino firmware: https://github.com/niekproductions/word-clock.git

_This instuction is kept for historical purposes_
This repo contains my code to control the clock. 
The firmware is flashed in two stages:
* at the hardware level, we use nodemcu(written in plain c, compiled against headers for the chip on the esp)
* nodemcu executes lua code which is stored in a simple filesystem on the system flash

You can build your own firmware at [nodemcu-build.com](http://nodemcu-build.com). Make sure you select RTC Time, SNTP, WiFi and WS2812. My firmware is included in the "woordklok-firmware" subdirectory.

```
esptool.py write_flash 0x00000 nodemcu-master-11-modules-2016-01-20-20-42-38-float.bin
```

If you are upgrading or flashing for the first time, it may be necessary to upgrade the init data. This does not come with the nodemcu-build.com firmware and has to be downloaded separately, see the [nodemcu documentation](http://nodemcu.readthedocs.io/en/latest/en/flash/#upgrading-firmware).

At this point you may want to fire up your favourite serial console application to check that you can get a terminal:
```
screen /dev/ttyUSB0 115200
>
```

```
python2 nodemcu-uploader.py --port /dev/ttyUSB0  node format
python2 nodemcu-uploader.py --port /dev/ttyUSB0  upload wordclock.lua
python2 nodemcu-uploader.py --port /dev/ttyUSB0  upload ini.lua:init.lua --restart
```


### setting up wifi ###

wifi.setmode(wifi.STATION)
wifi.sta.config("YOUR SSID","YOUR KEY")
wifi.sta.connect()
 
### Who do I talk to? ###
Any assistance will be gladly provided. If you need help sourcing parts, have questions about the code, the printing or anything else, please feel free to contact me:
* jeroenh@randomdata.nl
* jeroenh on #randomdata on freenode