#!/usr/bin/env python3
import cherrypy
import pytz
import datetime
import http
import geoip2.database
import json

class TZServer(object):
    @cherrypy.expose
    def index(self):
        ip = cherrypy.request.remote.ip,
        try:
            result = reader.city(ip[0])
        except geoip2.errors.AddressNotFoundError:
            offset = 0.0
        else:
            t = pytz.timezone(result.location.time_zone)
            offset = t.utcoffset(datetime.datetime.today()).seconds/3600
        return str(offset)

    @cherrypy.expose
    def utctime(self):
        # ip = cherrypy.request.remote.ip
        # for debugging:
        # ip = '82.170.27.134'
        # ip = '172.217.17.78'
        # try:
            # result = reader.city(ip)
        # except geoip2.errors.AddressNotFoundError:
            # t  = datetime.datetime.now()
        # else:
            # tz = pytz.timezone(result.location.time_zone)
            # t  = datetime.datetime.now(tz)
        t = datetime.datetime.utcnow()
        return str(json.dumps({"hours": t.hour, "minutes": t.minute, "seconds": t.second}))
        # return str(json.dumps({"hours": 20, "minutes": 50, "seconds": 7}))


    @cherrypy.expose
    def localtime(self):
        ip = cherrypy.request.remote.ip
        # for debugging:
        # ip = '82.170.27.134'
        # ip = '172.217.17.78'
        try:
            result = reader.city(ip)
        except geoip2.errors.AddressNotFoundError:
            t  = datetime.datetime.now()
        else:
            tz = pytz.timezone(result.location.time_zone)
            t  = datetime.datetime.now(tz)
        return str(json.dumps({"hours": t.hour, "minutes": t.minute, "seconds": t.second}))


    @cherrypy.expose
    def fakeweather(self):
        # return str(json.dumps({"hours": t.hour, "minutes": t.minute, "seconds": t.second}))
        # for debugging purposes: the first 3 values are discarded if you also set utctime to return str(json.dumps({"hours": 20, "minutes": 50, "seconds": 7}))
        # return str("{\"start\":1481056800,\"temp\":2,\"precip\":[0,0,0,0,0,50,0,70,0,0,0,0,0,0,57,62,0,0,0,0,0,0,0,0,0],\"levels\":{\"light\":0.25,\"moderate\":1,\"heavy\":2.5}}")
        return str("{\"start\":1481056800,\"temp\":2,\"precip\":[0,0,0,70,70,70,10,10,10,10,70,10,10,10,10,10,10,0,0,0,0,0,0,0,0],\"levels\":{\"light\":0.25,\"moderate\":1,\"heavy\":2.5}}")


if __name__ == '__main__':
    cherrypy.config.update({
        'server.socket_port': 8080,
        'server.socket_host': '0.0.0.0',
        'tools.proxy.on': True,
        'tools.proxy.base': 'http://timezone.1sand0s.nl'
    })
    global reader
    reader = geoip2.database.Reader("/usr/local/share/GeoIP/GeoLite2-City.mmdb")
    cherrypy.quickstart(TZServer())
