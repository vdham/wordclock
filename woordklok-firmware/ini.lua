TIMEZONE = 0

function init()
	ws2812.init()
	ws2812.write(string.char(0,0,0):rep(95))

	dofile("wordclock.lua")
	time_to_leds()
	
	-- wifi.sta.eventMonReg(wifi.STA_GOTIP, on_wifi_up)
	-- wifi.sta.eventMonStart()
	-- connect using stored credentials
	wifi.sta.connect()
	sntp.sync('ntp.pool.org')
	http.get("http://timezone.1sand0s.nl", nil, set_time_zone)
	tmr.alarm(0,1000,tmr.ALARM_AUTO, time_to_leds)
end


function set_time_zone(code,data)
    if (code < 0) then
      print("HTTP request failed")
    else
		print("Succeeded getting timezone:", data)
      TIMEZONE = data
    end
end


init() 
