-- read the docs: https://readthedocs.org/projects/nodemcu/
TIMEZONE = 1
RED_VALUE = 255
GREEN_VALUE = 255
BLUE_VALUE = 255

-- This is the mapping of words to the leds in the the clock
-- using the attached dutch clock face
-- including 4 dots at the bottom for extra minute indication

wordleds = {
	het = {85,86,87},
    is = {89,90},
	vijfm = {83,82,81,80},
	tienm = {79,78,77,76},
	kwart = {65,66,67,68,69},
	over = {71,72,73,74},
	voor = {63,62,61,60},
	half = {59,58,57,56},
	acht = {45,46,47,48},
	twee = {48,49,50,51},
	zes = {52,53,54},
	drie = {44,43,42,41},
	elf = {41,40,39},
	tien = {38,37,36},
	zeven = {26,27,28,29},
	negen = {29,30,31,32},
	vier = {24,23,22,21},
	twaalf = {20,19,18,17,16,15},
	een = {5,6,7},
	vijf = {8,9,10,11},
	uur = {12,13,14}
}
hourwords = {"een","twee","drie","vier", "vijf","zes","zeven","acht","negen","tien","elf","twaalf"}

function time_to_words() 
	local hourwords = {"een","twee","drie","vier", "vijf","zes","zeven","acht","negen","tien","elf","twaalf"}

	-- used for programming on PC, need rtctime on nodemcu
	-- local currenthour = hourwords[os.date("%I")/1]
	-- local nexthour = hourwords[os.date("%I")/1+1]
	-- local minutevalue = math.floor(os.date("%M")/5)


	local unix, micros
	unix,micros = rtctime.get()
	local currenthour = hourwords[math.floor((unix / 3600) + TIMEZONE) % 12]
	local nexthour = hourwords[math.floor(((unix / 3600) + TIMEZONE) % 12)+1]
	local minutevalue = (math.floor(unix/60) % 60) / 5


	if     minutevalue ==  0 then return("het is " .. currenthour .. " uur")
	elseif minutevalue ==  1 then return("het is vijfm over " .. currenthour)
	elseif minutevalue ==  2 then return("het is tienm over " .. currenthour)
	elseif minutevalue ==  3 then return("het is kwart over " .. currenthour)
	elseif minutevalue ==  4 then return("het is tienm voor half " .. nexthour)
	elseif minutevalue ==  5 then return("het is vijfm voor half " .. nexthour)
	elseif minutevalue ==  6 then return("het is half " .. nexthour)
	elseif minutevalue ==  7 then return("het is vijfm over half " .. nexthour)	
	elseif minutevalue ==  8 then return("het is tienm over half " .. nexthour)
	elseif minutevalue ==  9 then return("het is kwart voor " .. nexthour)
	elseif minutevalue == 10 then return("het is tienm voor " .. nexthour)
	elseif minutevalue == 11 then return("het is vijfm voor " .. nexthour)
	else	return "error"
	end
end

function time_to_leds() 

	-- used for programming on PC, need rtctime on nodemcu
	-- local currenthour = hourwords[os.date("%I")/1]
	-- local nexthour = hourwords[os.date("%I")/1+1]
	-- local minutevalue = math.floor(os.date("%M")/5)


	local unix, micros
	unix,micros = rtctime.get()
	local currenthour = hourwords[math.floor((unix / 3600) + TIMEZONE) % 12]
	local nexthour = hourwords[math.floor(((unix / 3600) + TIMEZONE) % 12)+1]
	local minutevalue = (math.floor(unix/60) % 60) / 5

	local result
	if     minutevalue ==  0 then
		--        het       is     uur
		result = {85,86,87, 89,90, 12,13,14}
		for k,v in pairs(wordleds[currenthour]) do table.insert(result,v) end
	elseif minutevalue ==  1 then
		--        het       is     vijf         over
		result = {85,86,87, 89,90, 83,82,81,80, 71,72,73,74}
		for k,v in pairs(wordleds[currenthour]) do table.insert(result,v) end
	elseif minutevalue ==  2 then
		--        het       is     tien          over
		result = {85,86,87, 89,90,  79,78,77,76, 71,72,73,74}
		for k,v in pairs(wordleds[currenthour]) do table.insert(result,v) end
	elseif minutevalue ==  3 then
		--        het       is     kwart           over
		result = {85,86,87, 89,90, 65,66,67,68,69, 71,72,73,74}
		for k,v in pairs(wordleds[currenthour]) do table.insert(result,v) end
	elseif minutevalue ==  4 then
		--        het       is     tien          voor         half
		result = {85,86,87, 89,90,  79,78,77,76, 63,62,61,60, 59,58,57,56}
		for k,v in pairs(wordleds[nexthour]) do table.insert(result,v) end
	elseif minutevalue ==  5 then
		--        het       is     vijf          voor         half
		result = {85,86,87, 89,90,  83,82,81,80, 63,62,61,60, 59,58,57,56}
		for k,v in pairs(wordleds[nexthour]) do table.insert(result,v) end
	elseif minutevalue ==  6 then
		--        het       is     half
		result = {85,86,87, 89,90, 59,58,57,56}
		for k,v in pairs(wordleds[nexthour]) do table.insert(result,v) end
	elseif minutevalue ==  7 then
		--        het       is     vijf          over         half
		result = {85,86,87, 89,90,  83,82,81,80, 71,72,73,74, 59,58,57,56}
		for k,v in pairs(wordleds[nexthour]) do table.insert(result,v) end
	elseif minutevalue ==  8 then
		--        het       is     tien          over         half
		result = {85,86,87, 89,90,  79,78,77,76, 71,72,73,74, 59,58,57,56}
		for k,v in pairs(wordleds[nexthour]) do table.insert(result,v) end
	elseif minutevalue ==  9 then
		--        het       is     kwart           voor        
		result = {85,86,87, 89,90, 65,66,67,68,69, 63,62,61,60}
		for k,v in pairs(wordleds[nexthour]) do table.insert(result,v) end
	elseif minutevalue == 10 then
		--        het       is     tien           voor        
		result = {85,86,87, 89,90, 79,78,77,76, 63,62,61,60}
		for k,v in pairs(wordleds[nexthour]) do table.insert(result,v) end
	elseif minutevalue == 11 then
		--        het       is     vijf           voor        
		result = {85,86,87, 89,90, 83,82,81,80, 63,62,61,60}
		for k,v in pairs(wordleds[nexthour]) do table.insert(result,v) end
	end
	bytes = {}
	for i=1,3*99 do
		bytes[i]=0
	end
	for i,k in pairs(result) do
		print(k)
		bytes[3*k-2] = RED_VALUE
		bytes[3*k-1] = GREEN_VALUE
		bytes[3*k] = BLUE_VALUE
	end
	return bytes
	local rgb = string.char(unpack(bytes))
	-- print(rgb)
	ws2812.write(2,rgb,3*99)
end
-- print(time_to_words())
-- print(unpack(time_to_leds()))
-- print("time: "..hours..":"..minutes..":"..seconds.."+"..millis)
-- leds = time_to_leds()




local rgb = string.char(unpack(bytes))
-- print(rgb)
ws2812.write(2,rgb,3*99)